---
title: "SkoolieHQ Documentation"
---

# SkoolieHQ Documentation

[SkoolieHQ](https://codeberg.org/skooliehq/) is digital dashboard for Skoolie
owners to montitor and analyze information about their Bus and house electrical
systems.

## Features

* Query J1939 vehicle network information
* Monitor RS485 information from common solar equipment
* Provides a digital dashboard for displaying information
* Review stored data with Grafana (analytics)
