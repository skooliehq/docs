---
title: "Description"
date: 2022-08-30T13:09:26-04:00
---

[SkoolieHQ](https://codeberg.org/skooliehq/) is a digital dashboard with the
primary purpose of monitoring a vehicles J1939 data network. Providing valuable
information about the health and status of the vehicle.

Using single board computers (SBC's) and various connectors we are able to 
query the bus's J1939 network for various mechanical statistics. Similar to a
cars ODB-II system for reading codes and other sensor data.

For solar related information the SBC can communicate either through blutooth
or through RS485.
